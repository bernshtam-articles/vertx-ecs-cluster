package com.valapay.vertxtest

import io.vertx.core.logging.LoggerFactory
import io.vertx.ext.web.Router
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle

class HttpVerticle : CoroutineVerticle() {

    private val logger = LoggerFactory.getLogger(HttpVerticle::class.java)
    private lateinit var ip:String

    @Throws(Exception::class)
    override suspend fun start() {
        val router = createRouter()

        ip = Main.getIp(logger)

        vertx.createHttpServer()
            .requestHandler(router)
            .listenAwait(PORT)

    }

    private fun createRouter(): Router = Router.router(vertx).apply {
        get("/health").handler { rc ->
            rc.response().end("ok")
            vertx.eventBus().send("AAA",ip)
            vertx.eventBus().send("BBB",ip)
        }
    }

    companion object {
        private const val PORT = 8080
    }

}
