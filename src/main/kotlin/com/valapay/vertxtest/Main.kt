package com.valapay.vertxtest

import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.eventbus.EventBusOptions
import io.vertx.core.json.JsonObject
import io.vertx.core.logging.Logger
import io.vertx.core.logging.LoggerFactory
import io.vertx.core.logging.SLF4JLogDelegateFactory
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager
import java.net.HttpURLConnection
import java.net.URL


object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        System.setProperty(LoggerFactory.LOGGER_DELEGATE_FACTORY_CLASS_NAME, SLF4JLogDelegateFactory::class.java.name)
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            LoggerFactory.getLogger(Main::class.java).error("Uncaught exception on thread {} ", e, t.name)
        }
        val logger = LoggerFactory.getLogger(Main::class.java)


        val mgr = HazelcastClusterManager()

        val options = VertxOptions().setClusterManager(mgr)
        val ebO = EventBusOptions()
        ebO.host = getIp(logger)
        ebO.port = 15701
        ebO.isClustered = true
        options.eventBusOptions = ebO

        Vertx.clusteredVertx(options) { res ->
            if (res.succeeded()) {
                logger.info("Deploy succeeded")
                val vertx = res.result()
                vertx.deployVerticle(AAAVerticle())
                vertx.deployVerticle(BBBVerticle())
                vertx.deployVerticle(HttpVerticle())
                logger.info("All verticles succeeded")

            } else {
                logger.error("Can't deploy verticles:", res.cause())
            }
        }


    }

    fun getIp(logger: Logger): String {
        return try {
            val body = JsonObject(getStat())
            logger.info("Got ${body.encodePrettily()}")
            val ip = body.getJsonArray("Containers").getJsonObject(0).getJsonArray("Networks")
                .getJsonObject(0)
                .getJsonArray("IPv4Addresses").getString(0)
            logger.info("Got IP to use: {}", ip)
            ip
        } catch (e: Exception) {
            logger.error("Cant fetch IP", e)
            "127.0.0.1"
        }
    }


    fun getStat(): String {
        val url = URL("http://169.254.170.2/v2/metadata")
        val httpURLConnection = url.openConnection() as HttpURLConnection
        return httpURLConnection.inputStream.bufferedReader().use { it.readText() }
    }
}