package com.valapay.vertxtest

import io.vertx.core.logging.LoggerFactory
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


abstract class Verticle(private val listenAddress: String) : CoroutineVerticle() {
    private val logger = LoggerFactory.getLogger(HttpVerticle::class.java)

    override suspend fun start() {
        val ip = Main.getIp(logger)
        vertx.eventBus().consumer<String>(listenAddress) { message ->
            GlobalScope.launch {
                logger.info("$ip: Got message from ${message.body()}")
            }
        }
    }


}